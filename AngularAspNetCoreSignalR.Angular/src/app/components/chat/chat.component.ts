import { Component, OnInit, OnDestroy} from '@angular/core';
import { HubConnection } from '@aspnet/signalr-client';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { SafeUrl } from '@angular/platform-browser/src/security/dom_sanitization_service';
declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, OnDestroy {

  constructor(private _sanitizer: DomSanitizer, private route:ActivatedRoute) {}

  private _hubConnection: HubConnection;
  private sub:any;
  user:any;
  file:any;
  nick = '';
  message = '';
  base64imgString = '';
  isDisabled= true;
  messages: {nick: string, message: string}[] = [];

  public sendMessage(): void {
    this._hubConnection
      .invoke('sendToAll', this.nick, this.message, this.base64imgString)
      .catch(err => console.error(err));
    this.message = '';
    this.base64imgString = '';
    this.isDisabled= true;
    $('#message').focus();
  }

  openImage(event){
    $('#image').click();
  }

  getFiles(event){ 
    var files = event.target.files;
    var file = files[0];
    this.file = file;
    console.log(file);
    if (files && file) {
      var reader = new FileReader();

      reader.onload =this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);
    }
  }

  inputChange(){
    if(this.message){
      this.isDisabled = false;
    }
    else{
      this.isDisabled = true;
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64imgString = 'data:'+ this.file.type+';base64,' +btoa(binaryString);
    this.isDisabled = false;
    this.sendMessage();
  }

  ngOnInit() {
    this.nick = window.prompt('Your name:', 'John');
    this.sub = this.route.params.subscribe(params => {
    this.user = params['user'];
  });

    this._hubConnection = new HubConnection('http://localhost:5000/chat');

    this._hubConnection
      .start()
      .then(() => console.log('Connection started!'))
      .catch(err => console.log('Error while establishing connection :('));

    $('#message').focus();
    this._hubConnection.on('sendToAll', (nick: string, receivedMessage: string, image:string) => {
      var santiImg = this._sanitizer.bypassSecurityTrustUrl(image);
      var obj:{nick:string, message:string, image:SafeUrl} = {"nick": nick, "message":receivedMessage, "image": santiImg};
      this.messages.push(obj);
      $(".messageContainer").animate({ scrollTop: $(document).height() }, "fast");
      return false;
    });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
