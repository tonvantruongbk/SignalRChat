import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { ChatComponent } from './components/chat/chat.component';
import { RegisterComponent } from './components/register/register.component';

import { AuthService } from './services/auth.service';
import { CheckAuthenticateService } from './services/check-authenticate.service';
import { LoginRedirectService } from './services/login-redirect.service';
import { HomeRedirectService } from './services/home-redirect.service';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ChatComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      { path: '', redirectTo: '/login', pathMatch: 'full', canActivate: [HomeRedirectService]},
      { path: 'login', component: LoginComponent, canActivate: [LoginRedirectService]},
      { path: 'chat', component: ChatComponent},
      { path: 'register', component: RegisterComponent}
    ])
  ],
  providers: [AuthService, CheckAuthenticateService, LoginRedirectService, HomeRedirectService],
  bootstrap: [AppComponent]
})
export class AppModule { }
