import { TestBed, inject } from '@angular/core/testing';

import { CheckAuthenticateService } from './check-authenticate.service';

describe('CheckAuthenticateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CheckAuthenticateService]
    });
  });

  it('should be created', inject([CheckAuthenticateService], (service: CheckAuthenticateService) => {
    expect(service).toBeTruthy();
  }));
});
